﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/MultiColorPoint" {
	Properties{
		_Color("Color", Color) = (1, 1, 1, 1)
		[Enum(False,0, True, 1)] _ColorOverride("Override Color", float) = 0
	}
	SubShader{
		Pass{
			LOD 200
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 _Color;
			float _ColorOverride;

			struct VertexInput {
				float4 v : POSITION;
				float4 color: COLOR;
			};

			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
			};

			VertexOutput vert(VertexInput v) {
				VertexOutput o;
				o.pos = UnityObjectToClipPos(v.v);
				o.color = v.color;
				return o;
			}

			float4 frag(VertexOutput output) : COLOR{

				if (_ColorOverride == 1)
				{
					output.color = _Color;
				}
				return output.color;
			}

			uniform float pointScale;

			float3 vertex_light_position;
			float4 eye_position;
			float sphere_radius;

			uniform float globalAlpha;
			ENDCG
		}
	}
	FallBack "Diffuse"
}
