﻿Shader "Custom/MultiColorCube" {
	Properties{
		//_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 1)
		[Enum(False,0, True, 1)] _ColorOverride("Override Color", float) = 0
		_PointSize("Point Size", float) = 1.0
		//[Enum(Point, 0, Square, 1, Cube, 2, Circle, 3)] _Shape("Point Shape", Float) = 2
		//[Enum(False,0, True, 1)] unif_W1("Use Lighting", float) = 0
	}
	SubShader{
		Pass{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#define UNITY_SHADER_NO_UPGRADE 1
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			# define VERT_COUNT 36
			float unif_W1;
			float _PointSize;

			float4 _MainTex_ST;
			sampler2D _MainTex;
			float3 vertex_light_position;

			float _ColorOverride;
			float4 _Color;
			float _Shape;

			struct VertexInput {
				float4 pos : POSITION;
				float4 col : COLOR;
			};

			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR0;
			};

			struct GeometryOut {
				float4 pos : SV_POSITION;
				float2 uv_MainTex : TEXCOORD0;
				float4 col : COLOR0;
				float3 nor : NORMAL;
			};

			VertexOutput vert(VertexInput input) {
				VertexOutput o;

				o.col = input.col;

				// Passing on center vertex (tile to be built by geometry shader from it later)
				o.pos = input.pos;

				return o;
			}

			half4 frag(GeometryOut output) : COLOR{

				float x = output.uv_MainTex.x;
				float y = output.uv_MainTex.y;
				float zz = 1.0 - x*x - y*y;

				//if (zz <= 0.0)
				//	discard;

				float z = sqrt(zz);

				float3 normal = float3(x, y, z);

				float4 col;

				col.rgb = output.col.rgb;
				col.a = output.col.a;

				if (_ColorOverride == 1)
				{
					col = _Color;
				}

				float3 lightDirection;
				float attenuation;
				if (unif_W1) // Use Lighting
				{
					// Lighting
					// https://en.wikibooks.org/wiki/Cg_Programming/Unity/Lighting_Textured_Surfaces
					if (0.0 == _WorldSpaceLightPos0.w) // directional light?
					{
						attenuation = 1.0; // no attenuation
						lightDirection = normalize(_WorldSpaceLightPos0.xyz);// -_WorldSpaceCameraPos.xyz);
					}
					float diffuse_value = max(dot(normal, vertex_light_position), 0.0);

					float3 ambientLighting =
						UNITY_LIGHTMODEL_AMBIENT.rgb * col.rgb;

					float3 diffuseReflection =
						attenuation * _LightColor0.rgb * col.rgb
						* max(0.0, dot(output.nor, lightDirection));

					col.rgb = col.rgb * ambientLighting + diffuseReflection;
				}

				return col;	
			}

			// https://takinginitiative.wordpress.com/2011/01/12/directx10-tutorial-9-the-geometry-shader/
			[maxvertexcount(VERT_COUNT)]
			void geom(point VertexOutput vert[1], inout TriangleStream<GeometryOut> triStream) {
				
				//if(_Shape == 2) { // Cube
					vertex_light_position = normalize(_WorldSpaceLightPos0.xyz - _WorldSpaceCameraPos.xyz);

					// Based on:
					// https://forum.unity3d.com/threads/simple-cube-shader.313644/
					float f = _PointSize / 2.0f; // _PointSize = 1 -> one meter

					const float4 vc[VERT_COUNT] = {
						float4(-f,  f,  f, 0.0f), float4(f,  f,  f, 0.0f), float4(f,  f, -f, 0.0f), // Top                                 
						float4(-f,  f,  f, 0.0f), float4(f,  f, -f, 0.0f), float4(-f,  f, -f, 0.0f),

						float4(f,  f,  f, 0.00), float4(f,  -f,  f, 0.00), float4(f, -f, -f, 0.00), // Front
						float4(f,  f,  f, 0.00), float4(f, -f, -f, 0.00), float4(f,  f, -f, 0.00),

						float4(-f,  f,  f, 0.00), float4(-f, -f,  f, 0.00), float4(f, -f,  f, 0.00), // Left
						float4(-f,  f,  f, 0.00), float4(f,  -f,  f, 0.00), float4(f,  f,  f, 0.00),

						float4(f,  f, -f, 0.00), float4(f, -f, -f, 0.00), float4(-f, -f, -f, 0.00), // Right                                  
						float4(f,  f, -f, 0.00), float4(-f, -f, -f, 0.00), float4(-f,  f, -f, 0.00),

						float4(-f,  f, -f, 0.00), float4(-f, -f, -f, 0.00), float4(-f, -f,  f, 0.00), // Back
						float4(-f,  f, -f, 0.00), float4(-f, -f,  f, 0.00), float4(-f,  f,  f, 0.00),

						float4(f, -f,  f, 0.00), float4(-f, -f,  f, 0.00), float4(-f, -f, -f, 0.00),
						float4(f, -f,  f, 0.00), float4(-f, -f, -f, 0.00), float4(f, -f, -f, 0.00)
					};


					const float2 UV1[VERT_COUNT] = {
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

						float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

						float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

						float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

						float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f)
					};


					const float3 NOR[VERT_COUNT] = {
					
						float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), // Left
						float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f),
					

						float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), // Back
						float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f),

						float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), // Bottom
						float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f),

						float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), // Top
						float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f),

						float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), // Front
						float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f),

						float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), // Right
						float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f)
					};

					const int TRI_STRIP[VERT_COUNT] = {
						0, 1, 2,  3, 4, 5,
						6, 7, 8,  9,10,11,
						12,13,14, 15,16,17,
						18,19,20, 21,22,23,
						24,25,26, 27,28,29,
						30,31,32, 33,34,35
					};

					GeometryOut v[VERT_COUNT];
					int i;

					// Assign new vertices positions 
					for (i = 0; i < VERT_COUNT; i++) {
						v[i].pos = vert[0].pos + vc[i];
						v[i].col = vert[0].col;
					}

					// Assign UV values
					for (i = 0; i < VERT_COUNT; i++) {
						// TRANSFORM_TEX "UnityCG.cginc"
						v[i].uv_MainTex = TRANSFORM_TEX(UV1[i], _MainTex);
					}

					// Assign Normal values
					for (i = 0; i < VERT_COUNT; i++) {
						v[i].nor = NOR[i];
					}

					// Position in VERT_COUNT space
					for (i = 0; i < VERT_COUNT; i++) {
						v[i].pos = UnityObjectToClipPos(v[i].pos);
					}

					// Build the cube tile by submitting triangle strip vertices
					for (i = 0; i < VERT_COUNT / 3; i++)
					{
						triStream.Append(v[TRI_STRIP[i * 3 + 0]]);
						triStream.Append(v[TRI_STRIP[i * 3 + 1]]);
						triStream.Append(v[TRI_STRIP[i * 3 + 2]]);

						triStream.RestartStrip();
					}
				//} else {
				/*	vertex_light_position = normalize(_WorldSpaceLightPos0.xyz - _WorldSpaceCameraPos.xyz);
					float f = _PointSize / 2.0f; // _PointSize = 1 -> one meter

					float3 up = normalize(cross(vert[0].pos, _WorldSpaceCameraPos));
					float3 look = _WorldSpaceCameraPos - vert[0].pos;
					look = normalize(look);
					float3 right = cross(up, look);

							
					float4 vv[4];
					vv[0] = float4(vert[0].pos + f * right - f * up, 1.0f);
					vv[1] = float4(vert[0].pos + f * right + f * up, 1.0f);
					vv[2] = float4(vert[0].pos - f * right - f * up, 1.0f);
					vv[3] = float4(vert[0].pos - f * right + f * up, 1.0f);

					// Based on:
					// https://forum.unity3d.com/threads/simple-cube-shader.313644/
					

					const float4 vc[6] = {
						float4(-f,  f,  f, 0.00), float4(-f, -f,  f, 0.00), float4(f, -f,  f, 0.00), // Left
						float4(-f,  f,  f, 0.00), float4(f,  -f,  f, 0.00), float4(f,  f,  f, 0.00)
					};


					const float2 UV1[6] = {
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
						float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					};


					const float3 NOR[6] = {
					
						float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), // Front
						float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f),
					};

					const int TRI_STRIP[6] = {
						0, 1, 2, 3, 4, 5
					};

					GeometryOut v[6];
					int i;

					// Assign new vertices positions 
					for (i = 0; i < 6; i++) {
						v[i].pos = vert[0].pos + vc[i];
						v[i].col = vert[0].col;
					}

					// Assign UV values
					for (i = 0; i < 6; i++) {
						// TRANSFORM_TEX "UnityCG.cginc"
						v[i].uv_MainTex = TRANSFORM_TEX(UV1[i], _MainTex);
					}

					// Assign Normal values
					for (i = 0; i < 6; i++) {
						v[i].nor = NOR[i];
					}

					// Position in VERT_COUNT space
					for (i = 0; i < 6; i++) {
						v[i].pos = UnityObjectToClipPos(v[i].pos);
					}

					// Build the cube tile by submitting triangle strip vertices
					triStream.Append(v[0]);
					triStream.Append(v[1]);
					triStream.Append(v[2]);
					triStream.RestartStrip();
					triStream.Append(v[3]);
					triStream.Append(v[4]);
					triStream.Append(v[5]);
				}*/
			}
			ENDCG
		}
	}
}
