﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeToPointLOD : MonoBehaviour {

    [SerializeField]
    Shader cubeShader;

    [SerializeField]
    Shader pointShader;

    public float transitionDistance = 50.0f;

    MeshRenderer[] meshRenderers;

    [SerializeField]
    float distance;

    [SerializeField]
    bool usePoints;

    // Use this for initialization
    void Start () {
        meshRenderers = GetComponentsInChildren<MeshRenderer>();

        StartCoroutine(CheckDistanceCR());
    }


    private void Update()
    {
        //CheckDistance();
    }

    IEnumerator CheckDistanceCR() {
        while (true)
        {
            CheckDistance();
            yield return new WaitForSeconds(1.0f);
        }
	}

    void CheckDistance()
    {
        foreach (MeshRenderer rm in meshRenderers)
        {
            distance = Vector3.Distance(Camera.main.transform.position, rm.transform.position);

            usePoints = distance > transitionDistance;
            rm.material.shader = usePoints ? pointShader : cubeShader;
        }
    }

    public void SetTransitionDistance(float dist)
    {
        transitionDistance = dist;
    }
}
