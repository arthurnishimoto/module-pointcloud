﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ButtonWithLoadingBar : MonoBehaviour
{
    Button button;

    [SerializeField]
    GameObject loadingBarRoot;

    [SerializeField]
    GameObject loadingBarProgressBar;

    [SerializeField]
    float loadProgess;

    float fadeOutSpeed = 0.5f;
    float fadeProgress;

    Image[] loadingBarImages;

    [SerializeField]
    string loadScene;

    bool hideBar;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.interactable = false;
        SetLoadingProgress(0);

        loadingBarImages = loadingBarRoot.GetComponentsInChildren<Image>();
    }

    void Update()
    {
        if(loadingBarRoot.activeSelf)
        {
            loadingBarProgressBar.transform.localScale = new Vector3(loadProgess, 1, 1);

            if(hideBar && fadeProgress < 1)
            {
                foreach (Image img in loadingBarImages)
                {
                    Color color = img.color;
                    color.a = 1 - fadeProgress;
                    img.color = color;
                }

                fadeProgress += Time.deltaTime * fadeOutSpeed;
                button.interactable = true;
            }
            else if (hideBar && fadeProgress >= 1)
            {
                loadingBarRoot.SetActive(false);
            }
        }
    }

    public void SetLoadingProgress(float value)
    {
        loadProgess = value;
    }

    public void SetLoadingProgress(float value, Color color)
    {
        loadProgess = value;
        loadingBarProgressBar.GetComponent<Image>().color = color;
    }

    public void EnableButton(bool value)
    {
        hideBar = value;
    }
    public void SetOnClickListener(UnityAction value)
    {
        button.onClick.AddListener(value);
        //loadScene = value;
    }

    public void SetButtonText(string value)
    {
        GetComponent<Button>().GetComponentInChildren<Text>().text = value;
    }
}
