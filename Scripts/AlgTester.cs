﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgTester : MonoBehaviour {

    [SerializeField]
    int numPoints = 200;

    [SerializeField]
    int limitPoints = 65;

    [SerializeField]
    int numPointGroups;

    [SerializeField]
    int maxPointsPerMesh = 10;

    // Use this for initialization
    void Start () {
        numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

        for (int id = 0; id < numPointGroups - 1; id++)
        {
            ProcessMesh(id, numPoints);
        }
        ProcessMesh(numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints);
    }
	
	void ProcessMesh (int id, int nPoints) {
        int meshSize = Mathf.Min(nPoints, maxPointsPerMesh);
        for (int i = 0; i < meshSize; ++i)
        {
            int index = id * maxPointsPerMesh + i;
            //myPoints[i] = points[id * limitPoints + i] - minValue;
            //indecies[i] = i;
            //myColors[i] = colors[id * limitPoints + i];
            Debug.Log("Added: " + id + "[" + i + "] = " + index);
        }
    }

}
