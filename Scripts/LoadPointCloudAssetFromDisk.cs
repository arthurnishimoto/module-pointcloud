﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoadInfo
{
    public string remotePath;
    public string localPath;
    public string assetFileName;
    public string publicName;
    public bool update = false;
    public bool showProgess = true;
    public ButtonWithLoadingBar button;
}

public class LoadPointCloudAssetFromDisk : MonoBehaviour
{
    [SerializeField]
    string headNodeServer = "http://10.0.0.175:4000/pointclouds";

    [SerializeField]
    string[] pointCloudSceneNames;

    //[SerializeField]
    //string assetBundlePathDisplayNode;

    //string pointCloudSceneAssetBundlePath;

    [SerializeField]
    string[] pointCloudSceneAssetBundleNames;

    [SerializeField]
    ButtonWithLoadingBar[] buttons;

    // Start is called before the first frame update
    void Start()
    {
        if(CAVE2.IsMaster())
        {
            //pointCloudSceneAssetBundlePath = assetBundlePathHeadNode;
        }
        else if(CAVE2.OnCAVE2Display())
        {
            //pointCloudSceneAssetBundlePath = assetBundlePathDisplayNode;
        }

        for (int i = 0; i < buttons.Length; i++)
        {
            LoadInfo loadInfo = new LoadInfo();

            loadInfo.remotePath = headNodeServer + "/" + pointCloudSceneAssetBundleNames[i];
            loadInfo.localPath = Application.persistentDataPath + "/" + pointCloudSceneAssetBundleNames[i];

            loadInfo.assetFileName = pointCloudSceneAssetBundleNames[i];
            loadInfo.publicName = pointCloudSceneNames[i];
            loadInfo.button = buttons[i];
            buttons[i].SetButtonText(loadInfo.publicName);

            StartCoroutine("VerifyFile", loadInfo);
            /*
            if (File.Exists(loadInfo.localPath))
            {
                Debug.Log("'" + loadInfo.localPath + "' exists, loading locally");
                //StartCoroutine("LoadAssetBundleSceneCR", loadInfo);
            }
            else
            {
                Debug.Log("'" + loadInfo.localPath + "' does not exists, downloading from '" + loadInfo.remotePath + "'");
                StartCoroutine("DownloadAssetBundleSceneCR", loadInfo);
            }
            */
            //StartCoroutine("LoadAssetBundleSceneCR", loadInfo);
            //StartCoroutine("DownloadAssetBundleSceneCR", loadInfo);
        }
    }

    IEnumerator VerifyFile(LoadInfo loadInfo)
    {
        if (File.Exists(loadInfo.localPath) && File.Exists(loadInfo.localPath + ".manifest"))
        {
            UnityWebRequest webRequest = UnityWebRequest.Get(loadInfo.remotePath + ".manifest");
            yield return webRequest.SendWebRequest();

            bool verified = false;

            if (!webRequest.isHttpError)
            {
                // Simple check of the CRC in the manifest
                // Note: Quick assumptions on input - TODO error checking
                string[] remoteManifestParse = webRequest.downloadHandler.text.Split('\n');
                string remoteCRCstr = remoteManifestParse[1].Split(' ')[1];

                string[] localManifestParse = File.ReadAllLines(loadInfo.localPath + ".manifest");
                string localCRCstr = remoteManifestParse[1].Split(' ')[1];

                int remoteCRC = 1;
                int localCRC = 0;
                if (int.TryParse(remoteCRCstr, out remoteCRC) && int.TryParse(localCRCstr, out localCRC))
                {
                    verified = localCRC == remoteCRC;
                }
            }
            else
            {
                verified = true;
            }

            if (verified)
            {
                if (webRequest.isHttpError)
                {
                    Debug.Log("'" + loadInfo.localPath + "' exists, CRC check failed to connect to server, loading locally");
                }
                else
                {
                    Debug.Log("'" + loadInfo.localPath + "' exists, CRC check passed, loading locally");
                }
                StartCoroutine("LoadAssetBundleSceneCR", loadInfo);
            }
            else
            {
                Debug.Log("'" + loadInfo.localPath + "' exists, CRC check failed, redownloading");
                loadInfo.update = true;
                StartCoroutine("DownloadAssetBundleSceneCR", loadInfo);

                LoadInfo manifestInfo = new LoadInfo();
                manifestInfo.remotePath = loadInfo.remotePath + ".manifest";
                manifestInfo.localPath = loadInfo.localPath + ".manifest";
                manifestInfo.showProgess = false;
                StartCoroutine("DownloadFileCR", manifestInfo);
            }
        }
        else
        {
            StartCoroutine("DownloadAssetBundleSceneCR", loadInfo);

            LoadInfo manifestInfo = new LoadInfo();
            manifestInfo.remotePath = loadInfo.remotePath + ".manifest";
            manifestInfo.localPath = loadInfo.localPath + ".manifest";
            manifestInfo.showProgess = false;
            StartCoroutine("DownloadFileCR", manifestInfo);
        }

        //return null;
    }

    IEnumerator DownloadFileCR(LoadInfo loadInfo)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(loadInfo.remotePath);
        webRequest.downloadHandler = new DownloadHandlerFile(loadInfo.localPath);
        webRequest.SendWebRequest();

        while (!webRequest.isDone)
        {
            yield return null;
        }
        if (webRequest.isHttpError)
        {

        }
        else
        {
            Debug.Log("Downloaded '" + loadInfo.remotePath + "'");
            Debug.Log("Saving to '" + loadInfo.localPath + "'");
        }
    }
    IEnumerator DownloadAssetBundleSceneCR(LoadInfo loadInfo)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(loadInfo.remotePath);
        webRequest.downloadHandler = new DownloadHandlerFile(loadInfo.localPath);
        webRequest.SendWebRequest();

        while (!webRequest.isDone)
        {
            //Debug.Log("Loading progress: " + (webRequest.downloadProgress * 100) + "%");
            if (loadInfo.showProgess)
            {
                loadInfo.button.SetLoadingProgress(webRequest.downloadProgress, new Color(0.01f, 0.51f, 0.01f));

                if (loadInfo.update)
                {
                    loadInfo.button.SetButtonText(loadInfo.publicName + " [UPDATING]");
                }
                else
                {
                    loadInfo.button.SetButtonText(loadInfo.publicName + " [DOWNLOADING]");
                }
            }
            yield return null;
        }
        if (webRequest.isHttpError)
        {
            if (loadInfo.showProgess)
            {
                loadInfo.button.SetButtonText(loadInfo.publicName + " [DOWNLOAD FAILED]");
                loadInfo.button.SetLoadingProgress(webRequest.downloadProgress, new Color(0.5f, 0.01f, 0.01f));
            }

            File.Delete(loadInfo.localPath);
        }
        else
        {
            Debug.Log("Downloaded '" + loadInfo.remotePath + "'");
            Debug.Log("Saving to '" + loadInfo.localPath + "'");

            StartCoroutine("LoadAssetBundleSceneCR", loadInfo);
        }
    }
    IEnumerator LoadAssetBundleSceneCR(LoadInfo loadInfo)
    {
        AssetBundleCreateRequest createRequest = AssetBundle.LoadFromFileAsync(loadInfo.localPath);

        while (!createRequest.isDone)
        {
            //Output the current progress
            //Debug.Log("Loading progress: " + (createRequest.progress * 100) + "%");
            if (loadInfo.showProgess)
            {
                loadInfo.button.SetLoadingProgress(createRequest.progress);
                loadInfo.button.SetButtonText(loadInfo.publicName + " [LOADING]");
            }
            yield return null;
        }

        //yield return createRequest;
        AssetBundle bundle = createRequest.assetBundle;

        /*
        foreach (string assetName in bundle.GetAllAssetNames())
        {
            Debug.Log(assetName);

            var prefab = bundle.LoadAsset<GameObject>(assetName);
            Instantiate(prefab);
        }
        */

        if (loadInfo.showProgess)
        {
            loadInfo.button.SetLoadingProgress(createRequest.progress);
            loadInfo.button.EnableButton(true);
            loadInfo.button.SetButtonText(loadInfo.publicName);

            foreach (string assetName in bundle.GetAllScenePaths())
            {
                Debug.Log(assetName);

                loadInfo.button.SetOnClickListener(delegate { LoadScene(assetName); });
                //SceneManager.LoadScene(assetName, LoadSceneMode.Single);
                //var prefab = bundle.LoadAsset<GameObject>(assetName);
                //Instantiate(prefab);
            }
        }
        //SceneManager.LoadScene(bundle.GetAllScenePaths()[0], LoadSceneMode.Single);
        //var prefab = bundle.LoadAsset<GameObject>("samplepointcloud-250k.xyzb");
        //Instantiate(prefab);
    }

    public void LoadScene(string scenePath)
    {
        SceneManager.LoadScene(scenePath, LoadSceneMode.Single);
    }
}
