﻿/**************************************************************************************************
* PointCloudGenerator.cs
 *-------------------------------------------------------------------------------------------------
 * Copyright 2016-2018		Electronic Visualization Laboratory, University of Illinois at Chicago
 * Author(s):										
 *  Arthur Nishimoto		anishimoto42@gmail.com
 *-------------------------------------------------------------------------------------------------
 * Copyright (c) 2016-2018, Electronic Visualization Laboratory, University of Illinois at Chicago
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the documentation and/or other 
 * materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************************/

using UnityEngine;
using System.Collections;
using System.IO;

public class PointCloudGenerator : MonoBehaviour {

    [SerializeField]
    string pointCloudfile;

    [SerializeField]
    Material matVertex;

    private int limitPoints = 65000;

    // Use this for initialization
    void Start () {
#if UNITY_EDITOR
        // Check if target directories exist, if not create
        if (!Directory.Exists(Application.dataPath + "/Resources/"))
            UnityEditor.AssetDatabase.CreateFolder("Assets", "Resources");

        if (!Directory.Exists(Application.dataPath + "/Resources/PointCloudMeshes/"))
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources", "PointCloudMeshes");

        if (!Directory.Exists(Application.dataPath + "/Resources/PointCloudMeshes/" + Path.GetFileName(pointCloudfile)))
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", Path.GetFileName(pointCloudfile));

#endif
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadPointCloudXYZB(string pointCloudfile)
    {
        StartCoroutine("loadXYZB", pointCloudfile);
    }

    // Start Coroutine of reading the points from the XYZB file (Binary file: x, y, z, r, g, b, a)
    private IEnumerator loadXYZB(string dPath)
    {
        int dataFields = 7;

        FileInfo fileInfo = new FileInfo(dPath);
        int calcuatedPointCount = (int)(fileInfo.Length / (sizeof(double) * dataFields));

        BinaryReader reader = new BinaryReader(File.Open(dPath, FileMode.Open));
        int numPoints = 0;
        ArrayList pointsAL = new ArrayList();
        ArrayList colorsAL = new ArrayList();

        float progress;
        while (true)
        {
            try
            {
                double x = reader.ReadDouble();
                double y = reader.ReadDouble();
                double z = reader.ReadDouble();

                double r = reader.ReadDouble();
                double g = reader.ReadDouble();
                double b = reader.ReadDouble();
                double a = reader.ReadDouble();

                pointsAL.Add(new Vector3((float)x, (float)y, (float)z));
                colorsAL.Add(new Color((float)r, (float)g, (float)b, (float)a));

                numPoints++;
            }
            catch
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }
            if (numPoints % 2000 == 0)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount) * 1.0f;
                yield return null;
            }
        }

        //Debug.Log("Loaded " + numPoints + " points from " + Path.GetFileName(dPath));
        Vector3[] points = (Vector3[])pointsAL.ToArray(typeof(Vector3));
        Color[] colors = (Color[])colorsAL.ToArray(typeof(Color));

        GeneratePointCloud(dPath, points, colors);
    }

    // Generate a point cloud gameobject as a prefab
    private void GeneratePointCloud(string dPath, Vector3[] points, Color[] colors)
    {
        Debug.Log("GeneratePointCloud: " + dPath);

        int numPoints = points.Length;
        Vector3 minValue = new Vector3();

        // Instantiate Point Groups
        int numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

        //Debug.Log(limitPoints + " exceeded. Building " + numPointGroups + " point groups.");
        GameObject pointCloud = new GameObject(Path.GetFileName(dPath));
        //pointCloud.transform.parent = GameObject.Find("World").transform;
        pointCloud.transform.localPosition = Vector3.zero;
        pointCloud.transform.localRotation = Quaternion.identity;
        pointCloud.transform.localScale = Vector3.one;

        for (int i = 0; i < numPointGroups - 1; i++)
        {
            InstantiateMesh(pointCloud, Path.GetFileName(dPath), i, limitPoints, points, colors, minValue);
        }
        InstantiateMesh(pointCloud, Path.GetFileName(dPath), numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints, points, colors, minValue);

        // Store PointCloud
#if UNITY_EDITOR
        UnityEditor.PrefabUtility.CreatePrefab("Assets/Resources/PointCloudMeshes/" + Path.GetFileName(dPath) + ".prefab", pointCloud);
#endif
    }

    private void InstantiateMesh(GameObject pointCloud, string filename, int meshInd, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue)
    {
        // Create Mesh
        GameObject pointGroup = new GameObject(filename + meshInd);
        pointGroup.AddComponent<MeshFilter>();
        pointGroup.AddComponent<MeshRenderer>();
        pointGroup.GetComponent<Renderer>().material = matVertex;

        pointGroup.GetComponent<MeshFilter>().mesh = CreateMesh(meshInd, nPoints, points, colors, minValue);
        pointGroup.transform.parent = pointCloud.transform;

        //pointGroup.transform.parent = GameObject.Find("World").transform;
        pointGroup.transform.localPosition = Vector3.zero;
        pointGroup.transform.localRotation = Quaternion.identity;
        pointGroup.transform.localScale = Vector3.one;

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter>().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    private Mesh CreateMesh(int id, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue)
    {
        //Debug.Log(" Point Group: " + id + " " + nPoints + "/" + points.Length);
        Mesh mesh = new Mesh();

        Vector3[] myPoints = new Vector3[nPoints];
        int[] indecies = new int[nPoints];
        Color[] myColors = new Color[nPoints];

        for (int i = 0; i < nPoints; ++i)
        {
            myPoints[i] = points[id * limitPoints + i] - minValue;
            indecies[i] = i;
            myColors[i] = colors[id * limitPoints + i];
        }

        mesh.vertices = myPoints;
        mesh.colors = myColors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);
        mesh.uv = new Vector2[nPoints];
        mesh.normals = new Vector3[nPoints];

        return mesh;
    }
}
