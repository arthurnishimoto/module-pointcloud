﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections;

// Unity Point Cloud Loader
// (C) 2016 Ryan Theriot, Eric Wu, Jack Lam. Laboratory for Advanced Visualization & Applications, University of Hawaii at Manoa.
// Version: January 9, 2018 by Arthur Nishimoto. Electronic Visualization Laboratory, University of Illinois at Chicago (Based on Version: February 17th, 2017)

public class PointCloudLoaderWindow : EditorWindow
{
    //Number of elements per data line from input file
    private static int elementsPerLine = 0;

    //Position of XYZ and RGB elements in data line
    private static int rPOS, gPOS, bPOS, xPOS, yPOS, zPOS;

    //Enumerator for PointCloud color range
    //None = No Color, Normalized = 0-1.0f, RGB = 0-255
    private enum ColorRange
    {
        NONE = 0,
        NORMALIZED = 1,
        RGB = 255
    }
    private static ColorRange colorRange;

    //Data line delimiter  
    public static string dataDelimiter;

    //Data line delimiter  
    public static string lodParameters = "65000 0.2:2 0.8:1";

    public static Material pointCloudMaterial;

    public static bool useLODGroups;

    //Maximum vertices a mesh can have in Unity
    static int limitPoints = 65000;

    [MenuItem("Window/PointClouds/LoadCloud")]
    private static void ShowEditor()
    {
        EditorWindow window = GetWindow(typeof(PointCloudLoaderWindow), true, "Point Cload Loader");
        window.maxSize = new Vector2(385f, 355f);
        window.minSize = window.maxSize;
    }

    //GUI Window Stuff - NO COMMENTS
    private void OnGUI()
    {
        GUIStyle help = new GUIStyle(GUI.skin.label);
        help.fontSize = 12;
        help.fontStyle = FontStyle.Bold;
        
        EditorGUILayout.LabelField("How To Use", help);

        EditorGUILayout.HelpBox("Note if loading an .xyzb file, skip to step 6.\n" +
                                "1. Set the number of elements that exist on each data line. \n" +
                                "2. Set the delimiter between each element. (Leave blank for white space) \n" +
                                "3. Set the index of the XYZ elements on the data line. (First element = 1) \n" +
                                "4. Select the range of the color data: \n" +
                                "       None: No Color Data \n" +
                                "       Normalized: 0.0 - 1.0 \n" +
                                "       RGB : 0 - 255 \n" +
                                "5. Set the index of the RGB elements on the data line. (First element = 1) \n" +
                                "6. Click \"Load Point Cloud File\"", MessageType.None);

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.BeginVertical();
        elementsPerLine = EditorGUILayout.IntField(new GUIContent("Elements Per Data Line", "The Number of Elements in the data line"), elementsPerLine);
        dataDelimiter = EditorGUILayout.TextField(new GUIContent("Data Line Delimiter", "Leave blank for white space between elements"), dataDelimiter);
        xPOS = EditorGUILayout.IntField(new GUIContent("X Index", "Index of X in data line"), xPOS);
        yPOS = EditorGUILayout.IntField(new GUIContent("Y Index", "Index of Y in data line"), yPOS);
        zPOS = EditorGUILayout.IntField(new GUIContent("Z Index", "Index of Z in data line"), zPOS);

        colorRange = (ColorRange)EditorGUILayout.EnumPopup(new GUIContent("Color Range", "None(No Color), Normalized (0.0-1.0f), RGB(0-255)"), colorRange);

        if (colorRange == ColorRange.NORMALIZED || colorRange == ColorRange.RGB)
        {
            rPOS = EditorGUILayout.IntField(new GUIContent("Red Index", "Index of Red color in data line"), rPOS);
            gPOS = EditorGUILayout.IntField(new GUIContent("Green Index", "Index of Green color in data line"), gPOS);
            bPOS = EditorGUILayout.IntField(new GUIContent("Blue Index", "Index of Blue color in data line"), bPOS);
        }
        pointCloudMaterial = (Material)EditorGUILayout.ObjectField("Material", pointCloudMaterial, typeof(Material), false);
        useLODGroups = EditorGUILayout.Toggle("Apply LODGroups", useLODGroups);

        lodParameters = EditorGUILayout.TextField(new GUIContent("LOD Parameters", "[MaxPointsPerBatch] minDist:maxDist:decimation minDist2:maxDist2:decimation2 etc."), lodParameters);

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        
        GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
        buttonStyle.fontSize = 12;
        buttonStyle.fontStyle = FontStyle.Bold;
        if (GUILayout.Button("Load Point Cloud File", buttonStyle, GUILayout.Height(50)))
        {
            LoadCloud();
        }

    }

    private void LoadCloud()
    {
        //Get path to file with EditorUtility
        string path = EditorUtility.OpenFilePanel("Load Point Cloud File", "", "");

        //If path doesn't exist of user exits dialog exit function
        if (path.Length == 0) return;

        //Set data delimiter
        char delimiter = ' ';
        try
        {
            if (dataDelimiter.Length != 0) delimiter = dataDelimiter.ToCharArray()[0];

        }
        catch (NullReferenceException)
        {
        }

        //Create string to name future asset creation from file's name
        string filename = null;
        string extension = "";
        try {
            string[] filename_ext = Path.GetFileName(path).Split('.');
            filename = Path.GetFileName(path);
            extension = filename_ext[1];
        }
        catch (ArgumentOutOfRangeException e) { Debug.LogError("PointCloudLoader: File must have an extension. (.pts, .xyz....etc)" + e); }

        
        
        //Create PointCloud Directories
        if (!Directory.Exists(Application.dataPath + "/PointClouds/"))
            AssetDatabase.CreateFolder("Assets", "PointClouds");

        if (!Directory.Exists(Application.dataPath + "/PointClouds/" + filename))
            UnityEditor.AssetDatabase.CreateFolder("Assets/PointClouds", filename);

        if (extension.ToLower() == "xyzb")
        {
            LoadXYZB(path);
            return;
        }
        
        //Setup Progress Bar 
        float progress = 0.0f;
        EditorUtility.ClearProgressBar();
        EditorUtility.DisplayProgressBar("Progress", "Percent Complete: " + (int)(progress * 100) + "%", progress);

        //Setup variables so we can use them to center the PointCloud at origin
        float xMin = float.MaxValue;
        float xMax = float.MinValue;
        float yMin = float.MaxValue;
        float yMax = float.MinValue;
        float zMin = float.MaxValue;
        float zMax = float.MinValue;

        //Streamreader to read data file
        StreamReader sr = new StreamReader(path);
        string line;

        //Could use a while loop but then cant show progress bar progression
        int numberOfLines = File.ReadAllLines(path).Length;
        int numPoints = 0;

        //For loop to count the number of data points (checks again elementsPerLine which is set by user)
        //Calculates the min and max of all axis to center point cloud at origin
        for (int i = 0; i < numberOfLines; i++)
        {
            line = sr.ReadLine();
            string[] words = line.Split(delimiter);

            //Only read data lines
            if (words.Length == elementsPerLine)
            {
                numPoints++;

                if (xMin > float.Parse(words[xPOS - 1]))
                    xMin = float.Parse(words[xPOS - 1]);
                if (xMax < float.Parse(words[xPOS - 1]))
                    xMax = float.Parse(words[xPOS - 1]);

                if (yMin > float.Parse(words[yPOS - 1]))
                    yMin = float.Parse(words[yPOS - 1]);
                if (yMax < float.Parse(words[yPOS - 1]))
                    yMax = float.Parse(words[yPOS - 1]);

                if (zMin > float.Parse(words[zPOS - 1]))
                    zMin = float.Parse(words[zPOS - 1]);
                if (zMax < float.Parse(words[zPOS - 1]))
                    zMax = float.Parse(words[zPOS - 1]);

            }

            //Update progress bar -Only updates every 10,000 lines - DisplayProgressBar is not efficient and slows progress
            progress = i * 1.0f / (numberOfLines - 1) * 1.0f;
            if (i % 10000 == 0)
                EditorUtility.DisplayProgressBar("Progress", "Percent Complete: " + (int)((progress * 100) / 3) + "%", progress / 3);

        }

        //Calculate origin of point cloud to shift cloud to unity origin
        float xAvg = (xMin + xMax) / 2;
        float yAvg = (yMin + yMax) / 2;
        float zAvg = (zMin + zMax) / 2;

        //Setup array for the points and their colors
        Vector3[] points = new Vector3[numPoints];
        Color[] colors = new Color[numPoints];

        //Reset Streamreader
        sr = new StreamReader(path);

        //For loop to create all the new vectors from the data points
        for (int i = 0; i < numPoints; i++)
        {
            line = sr.ReadLine();
            string[] words = line.Split(delimiter);

            //Only read data lines
            while (words.Length != elementsPerLine)
            {
                line = sr.ReadLine();
                words = line.Split(' ');
            }

            //Read data line for XYZ and RGB
            float x = float.Parse(words[xPOS - 1]) - xAvg;
            float y = float.Parse(words[yPOS - 1]) - yAvg;
            float z = (float.Parse(words[zPOS - 1]) - zAvg) * -1; //Flips to Unity's Left Handed Coorindate System
            float r = 1.0f;
            float g = 1.0f;
            float b = 1.0f;

            //If color range has been set also get color from data line
            if (colorRange == ColorRange.NORMALIZED || colorRange == ColorRange.RGB)
            {
                r = float.Parse(words[rPOS - 1]) / (int)colorRange;
                g = float.Parse(words[gPOS - 1]) / (int)colorRange;
                b = float.Parse(words[bPOS - 1]) / (int)colorRange;
            }

            //Save new vector to point array
            //Save new color to color array
            points[i] = new Vector3(x, y, z);
            colors[i] = new Color(r, g, b, 1.0f);

            //Update Progress Bar
            progress = i * 1.0f / (numPoints - 1) * 1.0f;
            if (i % 10000 == 0)
                EditorUtility.DisplayProgressBar("Progress", "Percent Complete: " + (int)(((progress * 100) / 3) + 33) + "%", progress / 3 + .33f);


        }

        //Close Stream reader
        sr.Close();


        // Instantiate Point Groups
        //Unity limits the number of points per mesh to 65,000.  
        //For large point clouds the complete mesh wil be broken down into smaller meshes
        int numMeshes = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

        //Create the new gameobject
        GameObject cloudGameObject = new GameObject(filename);

        //Create an new material using the point cloud shader
        Material newMat = new Material(Shader.Find("PointCloudShader"));
        //Save new Material
        AssetDatabase.CreateAsset(newMat, "Assets/PointClouds/" + filename + "Material" + ".mat");

        //Create the sub meshes of the point cloud
        for (int i = 0; i < numMeshes - 1; i++)
        {
            CreateMeshGroup(i, limitPoints, filename, cloudGameObject, points, colors, newMat);

            progress = i * 1.0f / (numMeshes - 2) * 1.0f;
            if (i % 2 == 0)
                EditorUtility.DisplayProgressBar("Progress", "Percent Complete: " + (int)(((progress * 100) / 3) + 66) + "%", progress / 3 + .66f);

        }
        //Create one last mesh from the remaining points
        int remainPoints = (numMeshes - 1) * limitPoints;
        CreateMeshGroup(numMeshes - 1, numPoints - remainPoints, filename, cloudGameObject, points, colors, newMat);

        progress = 100.0f;
        EditorUtility.DisplayProgressBar("Progress", "Percent Complete: " + progress + "%", 1.0f);

        //Store PointCloud
        UnityEditor.PrefabUtility.CreatePrefab("Assets/PointClouds/" + filename + ".prefab", cloudGameObject);
        EditorUtility.DisplayDialog("Point Cloud Loader", filename + " Saved to PointClouds folder", "Continue", "");
        EditorUtility.ClearProgressBar();
        
        return;
    }
    
    private void CreateMeshGroup(int meshIndex, int numPoints, string filename, GameObject pointCloud, Vector3[] points, Color[] colors, Material mat)
    {

        //Create GameObject and set parent
        GameObject pointGroup = new GameObject(filename + meshIndex);
        pointGroup.transform.parent = pointCloud.transform;

        //Add mesh to gameobject
        Mesh mesh = new Mesh();
        pointGroup.AddComponent<MeshFilter>();
        pointGroup.GetComponent<MeshFilter>().mesh = mesh;

        //Add Mesh Renderer and material
        pointGroup.AddComponent<MeshRenderer>();
        pointGroup.GetComponent<Renderer>().sharedMaterial = mat;

        //Create points and color arrays
        int[] indecies = new int[numPoints];
        Vector3[] meshPoints = new Vector3[numPoints];
        Color[] meshColors = new Color[numPoints];

        for (int i = 0; i < numPoints; ++i)
        {
            indecies[i] = i;
            meshPoints[i] = points[meshIndex * limitPoints + i];
            meshColors[i] = colors[meshIndex * limitPoints + i];
        }

        //Set all points and colors on mesh
        mesh.vertices = meshPoints;
        mesh.colors = meshColors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);

        //Create bogus uv and normals
        mesh.uv = new Vector2[numPoints];
        mesh.normals = new Vector3[numPoints];

        // Store Mesh
        UnityEditor.AssetDatabase.CreateAsset(mesh, "Assets/PointClouds/" + filename + @"/" + filename + meshIndex + ".asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();

        return;
    }
    
    // Start reading the points from the XYZB file (Binary file: x, y, z, r, g, b, a)
    private void LoadXYZB(string dPath)
    {
        int dataFields = 7;

        FileInfo fileInfo = new FileInfo(dPath);
        int calcuatedPointCount = (int)(fileInfo.Length / (sizeof(double) * dataFields));

        BinaryReader reader = new BinaryReader(File.Open(dPath, FileMode.Open));
        int numPoints = 0;
        ArrayList pointsAL = new ArrayList();
        ArrayList colorsAL = new ArrayList();

        float progress;
        EditorUtility.ClearProgressBar();

        double[] minMaxX = new double[] {Mathf.Infinity, Mathf.NegativeInfinity};
        double[] minMaxY = new double[] {Mathf.Infinity, Mathf.NegativeInfinity};
        double[] minMaxZ = new double[] {Mathf.Infinity, Mathf.NegativeInfinity};

        while (true)
        {
            try
            {
                double x = reader.ReadDouble();
                double y = reader.ReadDouble();
                double z = reader.ReadDouble();

                if (x < minMaxX[0])
                    minMaxX[0] = (float)x;
                else if (x > minMaxX[1])
                    minMaxX[1] = (float)x;
                if (y < minMaxY[0])
                    minMaxY[0] = (float)y;
                else if (y > minMaxY[1])
                    minMaxY[1] = (float)y;
                if (z < minMaxZ[0])
                    minMaxZ[0] = (float)z;
                else if (z > minMaxZ[1])
                    minMaxZ[1] = (float)z;

                double r = reader.ReadDouble();
                double g = reader.ReadDouble();
                double b = reader.ReadDouble();
                double a = reader.ReadDouble();

                pointsAL.Add(new Vector3((float)x, -(float)z, (float)y)); // Adjust for Unity coordinate system
                colorsAL.Add(new Color((float)r, (float)g, (float)b, (float)a));
                numPoints++;
            }
            catch
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }
            if (numPoints % 2000 == 0)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount) * 1.0f;
                EditorUtility.DisplayProgressBar("Reading xyzb ~" + calcuatedPointCount + " points", "Percent Complete: " + (int)((progress * 100)) + "%", progress);
                //yield return null;
            }
        }

        Vector3 avgPosition = new Vector3( (float)(minMaxX[0] + minMaxX[1]) / 2.0f, (float)(minMaxY[0] + minMaxY[1]) / 2.0f, (float)(minMaxZ[0] + minMaxZ[1]) / 2.0f);
        Vector3[] points = (Vector3[])pointsAL.ToArray(typeof(Vector3));
        Color[] colors = (Color[])colorsAL.ToArray(typeof(Color));

        GeneratePointCloud(dPath, points, colors, avgPosition);

        EditorUtility.DisplayDialog("Point Cloud Loader", dPath + " Saved to Resources/PointCloudMeshes folder", "Continue", "");
        EditorUtility.ClearProgressBar();
    }

    // Generate a point cloud gameobject as a prefab
    private void GeneratePointCloud(string dPath, Vector3[] points, Color[] colors, Vector3 centerPos)
    {
        Debug.Log("GeneratePointCloud: " + dPath);

        // Parse LOD info
        string[] lodParams = lodParameters.Split(' ');
        
        int maxPointsPerMesh;
        int.TryParse(lodParams[0], out maxPointsPerMesh);
        if(maxPointsPerMesh == 0)
        {
            maxPointsPerMesh = limitPoints;
        }

        ArrayList lodLevels = new ArrayList();
        for (int i = 1; i < lodParams.Length; i++)
        {
            string[] lodLevelParams = lodParams[i].Split(':');
            bool valid = false;
            float dist;
            int decimationLevel;
            valid = float.TryParse(lodLevelParams[0], out dist);
            valid = int.TryParse(lodLevelParams[1], out decimationLevel);
            if (valid)
            {
                lodLevels.Add(new Vector3(dist, decimationLevel, 0));
            }
        }
        if(lodLevels.Count == 0)
            lodLevels.Add(new Vector3(0.5f, 1, 0)); // Default

        Debug.Log("GeneratePointCloud: Using " + lodLevels.Count + " LOD levels. Max points per mesh: " + maxPointsPerMesh);
        
        int numPoints = points.Length;
        Vector3 minValue = new Vector3();

        // Instantiate Point Groups
        int numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

        //Debug.Log(limitPoints + " exceeded. Building " + numPointGroups + " point groups.");
        GameObject pointCloud = new GameObject(Path.GetFileName(dPath));
        //pointCloud.transform.parent = GameObject.Find("World").transform;
        pointCloud.transform.localPosition = Vector3.zero;
        pointCloud.transform.localRotation = Quaternion.identity;
        pointCloud.transform.localScale = Vector3.one;

        for (int i = 0; i < numPointGroups - 1; i++)
        {
            InstantiateMesh(pointCloud, Path.GetFileName(dPath), i, limitPoints, points, colors, minValue, maxPointsPerMesh, lodLevels);

            EditorUtility.DisplayProgressBar("Generating Point Groups", "Percent Complete: " + (int)((i / (float)numPointGroups * 100)) + "%", i / (float)numPointGroups);
        }
        InstantiateMesh(pointCloud, Path.GetFileName(dPath), numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints, points, colors, minValue, maxPointsPerMesh, lodLevels);

        // Store PointCloud
#if UNITY_EDITOR
        pointCloud.isStatic = true;
        UnityEditor.PrefabUtility.CreatePrefab("Assets/PointClouds/" + Path.GetFileName(dPath) + ".prefab", pointCloud);
#endif
    }

    private void InstantiateMesh(GameObject pointCloud, string filename, int meshInd, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue, int maxPointsPerMesh, ArrayList lodLevels)
    {
        // Create Mesh
        GameObject pointGroup = new GameObject(filename + meshInd);

        LOD[] lodStructs = new LOD[lodLevels.Count];
        LODGroup lodGroup = new LODGroup();
        if (useLODGroups)
        {
            lodGroup = pointGroup.AddComponent<LODGroup>();
            lodGroup.animateCrossFading = true;
        }

        Vector3 meshCenter = Vector3.zero;

        for (int i = 0; i < lodLevels.Count; i++)
        {
            Vector3 lodInfo = (Vector3)lodLevels[lodLevels.Count - 1 - i];
            float distance = lodInfo[0];
            int decimation = (int)lodInfo[1];

            GameObject pointLODGroup = new GameObject(filename + meshInd + "-LOD" + i + "-" + distance + ":" + decimation);

            pointLODGroup.AddComponent<MeshFilter>();
            pointLODGroup.AddComponent<MeshRenderer>();

            if(pointCloudMaterial == null)
                pointCloudMaterial = new Material(Shader.Find("Custom/MultiColorCube"));

            pointLODGroup.GetComponent<Renderer>().sharedMaterial = pointCloudMaterial;

            pointLODGroup.GetComponent<MeshFilter>().mesh = CreateMesh(meshInd, nPoints, points, colors, minValue, decimation);

            lodStructs[i] = new LOD(distance, pointLODGroup.GetComponents<Renderer>());

            meshCenter = pointLODGroup.GetComponent<MeshFilter>().sharedMesh.bounds.center;

            // Recalculate again for camera culling
            pointLODGroup.GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();

            pointLODGroup.transform.parent = pointGroup.transform;
            pointLODGroup.transform.localPosition = Vector3.zero;
            pointLODGroup.transform.localRotation = Quaternion.identity;
            pointLODGroup.transform.localScale = Vector3.one;
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.CreateAsset(pointLODGroup.GetComponent<MeshFilter>().sharedMesh, "Assets/PointClouds/" + filename + @"/" + filename + meshInd + "LOD" + i + ".asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        if (useLODGroups)
        {
            lodGroup.SetLODs(lodStructs);
            lodGroup.RecalculateBounds();
        }

        pointGroup.transform.parent = pointCloud.transform;
        pointGroup.transform.localPosition = meshCenter;
        pointGroup.transform.localRotation = Quaternion.identity;
        pointGroup.transform.localScale = Vector3.one;
    }

    private Mesh CreateMesh(int id, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue, int decimation)
    {
        //Debug.Log(" Point Group: " + id + " " + nPoints + "/" + points.Length);
        Mesh mesh = new Mesh();

        int decimatedNPoints = nPoints / decimation;
        Vector3[] myPoints = new Vector3[decimatedNPoints];
        int[] indecies = new int[decimatedNPoints];
        Color[] myColors = new Color[decimatedNPoints];

        int j = 0;
        for (int i = 0; i < nPoints; ++i)
        {
            if (i % decimation == 0)
            {
                myPoints[j] = points[id * limitPoints + i] - minValue;
                indecies[j] = j;
                myColors[j] = colors[id * limitPoints + i];
                j++;
            }
        }

        mesh.vertices = myPoints;
        mesh.colors = myColors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);
        mesh.uv = new Vector2[decimatedNPoints];
        mesh.normals = new Vector3[decimatedNPoints];
        mesh.RecalculateBounds();

        // Recenter mesh
        for (int i = 0; i < decimatedNPoints; ++i)
        {
            myPoints[i] = myPoints[i] - mesh.bounds.center;
        }
        mesh.vertices = myPoints;
        return mesh;
    }

}
